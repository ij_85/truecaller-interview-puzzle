package com.truecaller.interview.useraudit.resource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import com.truecaller.interview.useraudit.UserAuditApplication;
import com.truecaller.interview.useraudit.config.UserAuditServiceConfiguration;
import com.truecaller.interview.useraudit.dao.UserViewsDAO;
import com.truecaller.interview.useraudit.dto.Viewer;
import io.dropwizard.jackson.Jackson;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.skife.jdbi.v2.DBI;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import java.sql.Timestamp;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/*
* These rudimentary tests are here to test basic api handling of the post endpoint for storing views and
* the get endpoint for listing recent views.
* */
public class UserViewsResourceTest {

   @ClassRule
   public static final DropwizardAppRule<UserAuditServiceConfiguration> RULE =
      new DropwizardAppRule<UserAuditServiceConfiguration>(UserAuditApplication.class, "local-db-inmemory-configuration.yaml");

   private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

   private static final Client client = new Client();

   private static final WebResource viewsResource(int resource) {
      return client.resource(customerBaseURL + resource);
   }

   private static String customerBaseURL;
   private static UserViewsDAO userViewsDAO;

   @Before
   public void setup() throws JsonProcessingException, ClassNotFoundException {
      customerBaseURL = String.format("http://localhost:%d/views/", RULE.getLocalPort());

      DBIFactory factory = new DBIFactory();
      DBI jdbi = factory.build(RULE.getEnvironment(), RULE.getConfiguration().getDatabaseSourceFactory(), "sql");
      userViewsDAO = new UserViewsDAO(jdbi);
   }

   @After
   public void cleanup() {
   }

   @Test
   public void testCreateView() throws Exception {
      int viewedId = 1;
      int viewerId = 2;
      final Viewer viewToBeRegistered = new Viewer(viewerId, now());
      String viewAsJson = MAPPER.writeValueAsString(viewToBeRegistered);

      viewsResource(viewedId)
         .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
         .post(viewAsJson);

      List<Viewer> viewers = viewsResource(viewedId).get(new GenericType<List<Viewer>>() {
      });


      List<Viewer> filteredViews = viewers.stream().filter(new Predicate<Viewer>() {
         @Override
         public boolean test(Viewer view) {
            return viewToBeRegistered.equals(view);
         }
      }).collect(Collectors.toList());

      Assert.assertFalse(filteredViews.isEmpty());
   }

   //TODO: clean up and split into more granular tests
   @Test
   public void testListRecent() throws Exception {
      int viewedId = 2;

      for( int i = 0; i < 10; i++) {
         viewsResource(viewedId)
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
            .post(MAPPER.writeValueAsString(new Viewer(randomInt(), now())));
      }

      List<Viewer> viewers =
         viewsResource(viewedId)
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
            .get(new GenericType<List<Viewer>>() {
            });

      Assert.assertTrue(viewers.size() == 10);


      //Not recent timestamp should not be listed in recent views
      Timestamp notRecentTimestamp = new Timestamp(0L);

      viewsResource(viewedId)
         .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
         .post(MAPPER.writeValueAsString(new Viewer(randomInt(), notRecentTimestamp)));

      viewers =
         viewsResource(viewedId)
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
            .get(new GenericType<List<Viewer>>() {
            });

      Assert.assertTrue(viewers.stream().filter(new Predicate<Viewer>() {
         @Override
         public boolean test(Viewer viewer) {
            return viewer.getTime().equals(notRecentTimestamp);
         }
      }).collect(Collectors.toList()).isEmpty());


      //Very recent timestamp should be listed in recent views
      Timestamp recentTimestamp = now();

      viewsResource(viewedId)
         .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
         .post(MAPPER.writeValueAsString(new Viewer(randomInt(), recentTimestamp)));

      viewers =
         viewsResource(viewedId)
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
            .get(new GenericType<List<Viewer>>() {
            });

      Assert.assertFalse(viewers.stream().filter(new Predicate<Viewer>() {
         @Override
         public boolean test(Viewer viewer) {
            return viewer.getTime().equals(recentTimestamp);
         }
      }).collect(Collectors.toList()).isEmpty());


      //Old impression for viewedId with less than 10 impressions should not appear in recent list
      int viewedIdWithNoViews = 6;
      viewsResource(viewedIdWithNoViews)
         .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
         .post(MAPPER.writeValueAsString(new Viewer(randomInt(), new Timestamp(0))));

      viewers =
         viewsResource(viewedIdWithNoViews)
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
            .get(new GenericType<List<Viewer>>() {
            });

      Assert.assertTrue(viewers.isEmpty());
   }

   //TODO: test purge
   
   private Timestamp now() {
      return new Timestamp(DateTime.now().getMillis());
   }

   private int randomInt() {
      return (int) Math.random()*10000;
   }
}
;