package com.truecaller.interview.useraudit;

import com.truecaller.interview.useraudit.config.UserAuditServiceConfiguration;
import com.truecaller.interview.useraudit.dao.UserViewsDAO;
import com.truecaller.interview.useraudit.resource.UserViewsResource;
import com.truecaller.interview.useraudit.task.PurgeTask;
import io.dropwizard.Application;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.skife.jdbi.v2.DBI;

public class UserAuditApplication extends Application<UserAuditServiceConfiguration> {

   @Override
   public void initialize(Bootstrap<UserAuditServiceConfiguration> userAuditServiceConfigurationBootstrap) {
      userAuditServiceConfigurationBootstrap.addBundle(new MigrationsBundle<UserAuditServiceConfiguration>() {
         @Override
         public DataSourceFactory getDataSourceFactory(UserAuditServiceConfiguration configuration) {
            return configuration.getDatabaseSourceFactory();
         }
      });
   }

   @Override
   public void run(UserAuditServiceConfiguration userAuditServiceConfiguration, Environment environment) throws Exception {

      final DBIFactory factory = new DBIFactory();
      final DBI jdbi = factory.build(environment, userAuditServiceConfiguration.getDatabaseSourceFactory(), "sql");
      final UserViewsDAO userViewsDAO = new UserViewsDAO(jdbi);

      if (userAuditServiceConfiguration.isNewSchemaNeeded()) {
         userViewsDAO.setup();
      }

      final UserViewsResource userViewsResource = new UserViewsResource(userViewsDAO);

      environment.jersey().register(userViewsResource);

      environment.admin().addTask(new PurgeTask(userViewsDAO));
   }

   public static void main(String[] args) throws Exception {
      new UserAuditApplication().run(args);
   }
}
