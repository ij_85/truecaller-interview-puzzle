package com.truecaller.interview.useraudit.dto;

import com.truecaller.interview.useraudit.dbmodel.UserView;
import org.joda.time.DateTime;

import java.sql.Timestamp;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Viewer {

   private int userId;

   private Timestamp time = new Timestamp(DateTime.now().getMillis());

   public static List<Viewer> fromUserViews(List<UserView> userViews) {
      return userViews.stream().map(new Function<UserView, Viewer>() {
         @Override
         public Viewer apply(UserView userView) {
            return fromUserView(userView);
         }
      }).collect(Collectors.toList());
   }

   public static Viewer fromUserView(UserView userView) {
      return new Viewer(userView.getUserViewer(), userView.getTime());
   }

   //needed for jackson serialization
   public Viewer() {}

   public Viewer(int userId, Timestamp time) {
      this.userId = userId;
      this.time = time;
   }

   public int getUserId() {
      return userId;
   }

   public void setUserId(int userId) {
      this.userId = userId;
   }

   public Timestamp getTime() {
      return time;
   }

   public void setTime(Timestamp time) {
      this.time = time;
   }

   @Override
   public boolean equals(Object obj) {
      if ((obj == null) || !(obj instanceof Viewer)) {
         return false;
      }
      Viewer comparedViewer = (Viewer) obj;
      return (this.getUserId() == comparedViewer.getUserId()) && (this.getTime().equals(comparedViewer.getTime()));
   }
}
