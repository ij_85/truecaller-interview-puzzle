package com.truecaller.interview.useraudit.task;

import com.google.common.collect.ImmutableMultimap;
import com.truecaller.interview.useraudit.dao.UserViewsDAO;
import io.dropwizard.servlets.tasks.Task;

import java.io.PrintWriter;
import java.util.stream.IntStream;

public class PurgeTask extends Task {

   private UserViewsDAO userViewsDAO;

   public PurgeTask(UserViewsDAO userViewsDAO) {
      super("purge");
      this.userViewsDAO = userViewsDAO;
   }

   @Override
   public void execute(ImmutableMultimap<String, String> stringStringImmutableMultimap, PrintWriter printWriter) throws Exception {
      int newestViewedId = userViewsDAO.getNewestViewedId();
      IntStream.rangeClosed(0, newestViewedId).forEach(
         (i) -> userViewsDAO.cleanUp(i)
      );
   }
}
