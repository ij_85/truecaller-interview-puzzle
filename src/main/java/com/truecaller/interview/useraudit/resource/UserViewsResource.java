package com.truecaller.interview.useraudit.resource;

import com.truecaller.interview.useraudit.dao.UserViewsDAO;
import com.truecaller.interview.useraudit.dbmodel.UserView;
import com.truecaller.interview.useraudit.dto.Viewer;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/views/")
@Produces(MediaType.APPLICATION_JSON)
public class UserViewsResource {

   private UserViewsDAO userViewsDAO;

   public UserViewsResource(UserViewsDAO userViewsDAO) {
      this.userViewsDAO = userViewsDAO;
   }

   @POST
   @Path("/{viewedId}")
   @Consumes(MediaType.APPLICATION_JSON)
   public Response create(@PathParam("viewedId") int viewed, Viewer viewer) {
      UserView userView = new UserView(viewed, viewer.getUserId(), viewer.getTime());
      userViewsDAO.create(userView);

      //we are omitting returning the newly created view for performance, since it would cost a select query
      return Response.ok("1").build();
   }

   @GET
   @Path("/{viewedId}")
   public Response listRecent(@PathParam("viewedId") int viewedId) {
      List<UserView> userViews = userViewsDAO.getRecentViews(viewedId);
      return Response.ok(Viewer.fromUserViews(userViews)).build();
   }
}
