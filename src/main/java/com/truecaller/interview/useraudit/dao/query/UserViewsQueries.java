package com.truecaller.interview.useraudit.dao.query;

import com.truecaller.interview.useraudit.dbmodel.UserView;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.sql.Timestamp;
import java.util.List;


//I felt like trying something new like jdbi just to make it interesting, so stuff might not be optimal.
@UseStringTemplate3StatementLocator("/sql/userviews_queries.sql.stg")
@RegisterMapperFactory(BeanMapperFactory.class)
public interface UserViewsQueries {

   @SqlQuery
   UserView selectById(@Bind("id") String id);

   @SqlQuery
   List<UserView> listViewsForUser(@Bind("viewedId") int viewedId, @Bind("timeLimit") Timestamp timeLimit, @Bind("countLimit") int countLimit);

   @SqlUpdate
   @GetGeneratedKeys
   Integer create(@BindBean("v") UserView userView);

   @SqlUpdate
   void cleanUp(@Bind("viewedId") int viewedId, @Bind("timeLimit") Timestamp timeLimit, @Bind("countLimit") int countLimit);

   //This query is not optimal, but I have had some issues returning an int directly.
   @SqlQuery
   UserView getMaxViewed();
}
