package com.truecaller.interview.useraudit.dao;

import com.truecaller.interview.useraudit.dao.query.SetupDatabase;
import com.truecaller.interview.useraudit.dao.query.UserViewsQueries;
import com.truecaller.interview.useraudit.dbmodel.UserView;
import org.joda.time.DateTime;
import org.skife.jdbi.v2.DBI;

import java.sql.Timestamp;
import java.util.List;

public class UserViewsDAO {

   private static final int dayLimit = 10;

   private static final int viewCountLimit = 10;

   private UserViewsQueries userViewQueries;

   private SetupDatabase userViewSchema;

   public UserViewsDAO(DBI jdbi) {
      this.userViewQueries = jdbi.onDemand(UserViewsQueries.class);
      this.userViewSchema = jdbi.onDemand(SetupDatabase.class);
   }

   //not exposed through the api
   public UserView getById(String id) {
      return userViewQueries.selectById(id);
   }

   public Integer create(UserView userView) {
      Integer createdId = this.userViewQueries.create(userView);
      return createdId;
   }

   public List<UserView> getRecentViews(int viewedId) {
      Timestamp tenDaysAgo = new Timestamp(DateTime.now().minusDays(dayLimit).getMillis());
      return userViewQueries.listViewsForUser(viewedId, tenDaysAgo, viewCountLimit);
   }

   public void cleanUp(int viewedId) {
      Timestamp tenDaysAgo = new Timestamp(DateTime.now().minusDays(dayLimit).getMillis());
      userViewQueries.cleanUp(viewedId, tenDaysAgo, viewCountLimit);
   }

   public int getNewestViewedId() {
      return userViewQueries.getMaxViewed().getUserViewed();
   }

   public void setup() {
      userViewSchema.createSchema();
   }
}
