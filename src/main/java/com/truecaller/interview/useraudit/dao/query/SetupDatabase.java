package com.truecaller.interview.useraudit.dao.query;

import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;

@UseStringTemplate3StatementLocator("/sql/userviews_setup_schema.sql.stg")
public interface SetupDatabase {

   @SqlUpdate
   void createSchema();
}
