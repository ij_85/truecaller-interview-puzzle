package com.truecaller.interview.useraudit.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class UserAuditServiceConfiguration extends Configuration {

   @Valid
   @NotNull
   @JsonProperty
   private DataSourceFactory database = new DataSourceFactory();

   public DataSourceFactory getDatabaseSourceFactory() {
      return database;
   }

   @Valid
   @NotNull
   @JsonProperty
   private boolean setupSchema = false;

   public boolean isNewSchemaNeeded() {
      return setupSchema;
   }
}
