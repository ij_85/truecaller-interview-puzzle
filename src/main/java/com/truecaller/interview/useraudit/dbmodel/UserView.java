package com.truecaller.interview.useraudit.dbmodel;

import java.sql.Timestamp;

public class UserView {
   private int id;

   private int userViewed;

   private int userViewer;

   private Timestamp time;

   public UserView() {

   }

   public UserView(int userViewed, int userViewer, Timestamp time) {
      this.userViewed = userViewed;
      this.userViewer = userViewer;
      this.time = time;
   }

   public int getId() {
      return id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public int getUserViewed() {
      return userViewed;
   }

   public void setUserViewed(int userViewed) {
      this.userViewed = userViewed;
   }

   public int getUserViewer() {
      return userViewer;
   }

   public void setUserViewer(int userViewer) {
      this.userViewer = userViewer;
   }

   public Timestamp getTime() {
      return time;
   }

   public void setTime(Timestamp time) {
      this.time = time;
   }
}
