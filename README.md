# README #

This code has been written for the Truecaller interview puzzle. See puzzle.txt file for details.

### Introduction ###

The problem described in puzzle.txt is essentially the problem of storing and retrieving a timeseries. 
The problem statement requires, apparently, the use of a relational database as backend though it should be noted that technologies "no-sql" technologies like Redis and Cassandra are more suited to this kind of data schema, in particular in the context of high write volumes and data sample expiration.

DISCLAIMER: given that the puzzle states 2 hours as the amount of time to be dedicated to solving it, most shortcomings of the code are due to not spending over 2 hours improving it. 


### Relational schema overview ###
Given what was mentioned in the introduction, the design of the schema was heavily conditioned by the desire to have fast writes.

The relational schema is composed of a single table, described by the followng SQL data manipulation statement:

~~~
CREATE TABLE userview (id INTEGER CONSTRAINT PK_USERVIEW PRIMARY KEY AUTOINCREMENT NOT NULL, viewed INTEGER NOT NULL, viewer INTEGER NOT NULL, time TEXT NOT NULL);
CREATE INDEX index1 ON userview(viewed, time);
~~~

The table has the least number of foreign key constraints specified in order to avoid reference checks during writes.

The index has been introduced in order to allow for decent performance of the "list recent views" api described below in this document, avoiding costly table scans to satisfy the "recent views" api. Hence some space (and in smaller part some write performance) has been sacrificed for read performance.

The dataset contained in the database is subject to maintenance operations accessible through the relevant api call described in the api description section.
 

### How to run the project ###

Once the repository or folder has been downloaded, from it's root directory one can build it with maven. In particular the project has been built with java 1.8.0_05, maven 3.1.1 and sqlite 3.8.5. Make sure port 8080 and 8081 are available prior to building and running and that you have write permissions in the working directory, and that the root directory of the repository is the working directory.

To build the project (and execute tests) run:
~~~
mvn package
~~~
To run the project first create the local database by running: 
~~~
java -jar ./target/truecaller-puzzle-1.0-SNAPSHOT.jar db migrate local-db-onfile-configuration.yaml
~~~
then start the server with:
~~~
java -jar ./target/truecaller-puzzle-1.0-SNAPSHOT.jar server local-db-onfile-configuration.yaml
~~~

The application will create a local database stored in the file `userviews.db` placed in the root of the repository. This database can be queried manually using the sqlite application.

### Api description ###
The api of the "Truecaller User Views Audit" application talks json, in particular it is able to parse and return jsons structured mapped by the Viewer class, structured as follows:
~~~
{"userId": "6", "time": "1408312243650"}
~~~
This json represents a view by user with id 6 at Sun, 17 Aug 2014 21:50:43 UTC.

Posting this json to `localhost:8080/views/1` will ensure that a view by user 6 on user 1 at Sun, 17 Aug 2014 21:50:43 UTC is stored in the database. A http get method sent to `localhost:8080/views/1` will list the 10 most recent Viewers for user 1 with time stamp not older than 10 days.

The tests contained in UserViewResourceTest.java show how to interact with the api through a java http client.

Finally a clean up operation trigger has been exposed through the dropwizard tasks framework. Performing a http post request to the `localhost:8081/tasks/purge` with an empty body will trigger the deletion of user views not needed any more in the database. This operation could be triggered periodically with a pattern which takes into account how traffic develops over time, for example if during the night low traffic is experienced, it might be a good time to perform this operation then. This code is more here just to give an idea, there are considerations to be made whether it can run problem free with simultaneous new views being generated.

### Code layout ###
The application java source code can be found under `src/main/java`. This code is all under the root package `com.truecaller.interview.useraudit` and is further distributed in the following packages:
~~~
config
~~~
for the classes mapping the dropwizard yaml configuration file
~~~
dao
~~~
for the classes used to access the database
~~~
dbmodel
~~~
for the classes used to map entities stored in the database
~~~
dto
~~~
for the classes used to map json entities which the api is able to parse and return
~~~
resource
~~~
for the classes used to specifies code handling the api requests.
~~~
task
~~~
for the classes which implement utility functionality needed by the application

The application textual resources can be found under `src/main/resources`. Here we can find migration files and sql query statements referred to by the java code.
Finally under `src/test/java` we can find the source code of some tests.